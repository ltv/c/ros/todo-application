import Service from '@/service';
import { Todo } from '@/store';

export class HomeService extends Service {
  public getTodoList(completed?: boolean): Promise<Todo[]> {
    // return [...{Todo}]
    let params = {};
    if (completed !== undefined && completed !== null) {
      params = { completed: !!completed }; // completed = empty, 1: true, 0: false
    }
    return this.get('/todos', params);
  }

  public createTodo(todo: Todo): Promise<Todo> {
    return this.post('/createTodo', todo);
  }

  public updateTodo(todo: Todo): Promise<Todo> {
    return this.post('/updateTodo', todo);
  }

  public deleteTodo(id: number): Promise<boolean> {
    return this.post('/deleteTodo', { id });
  }
}
