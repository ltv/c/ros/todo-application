const todoModel = require('../models/todo');

exports.createTodo = (req, res) => {
  const todo = req.body;
  const inserted = todoModel.addTodo(todo);
  res.json(inserted);
};

exports.updateTodo = (req, res) => {
  const todo = req.body;
  const updated = todoModel.updateTodoById(todo);
  res.json(updated);
};

exports.getTodos = (req, res) => {
  const completed = req.query.completed;
  const todos = todoModel.findAll({ completed });
  res.json(todos);
};

exports.deleteTodo = (req, res) => {
  const { id } = req.params;
  const result = todoModel.deleteById(id);
  res.json(result);
};

exports.clearCompleted = (_, res) => {
  const completed = todoModel.findAll({ completed: true });
  completed.forEach(todo => {
    todoModel.deleteById(todo.id);
  });
  res.json({ result: true });
};
