const express = require('express');
const router = express.Router();
const { createTodo, updateTodo, getTodos, deleteTodo, clearCompleted } = require('../controllers/todo');

router.post('/createTodo', createTodo);
router.post('/updateTodo', updateTodo);
router.get('/todos', getTodos);
router.post('/deleteTodo', deleteTodo);
router.post('/clearCompleted', clearCompleted);

module.exports = router;
