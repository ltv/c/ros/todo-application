const bodyParser = require('body-parser');
const cors = require('cors');
const router = require('../routers/todo');

module.exports = app => {
  app.set('port', 3000);
  app.set('json spaces', 4);
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use('/api', router);
};
