const allRoutes = require('express-list-endpoints');

module.exports = app => {
  app.listen(app.get('port'), () => {
    console.log(`My Todo App is listening on port ${app.get('port')}`);
    console.log('Registered Routes: ');
    console.log(allRoutes(app));
  });
};
