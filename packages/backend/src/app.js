const express = require('express');
const consign = require('consign');

const app = express();

consign()
  .include('src/libs/middlewares.js')
  // .then('src/routers')
  .then('src/controllers')
  .then('src/models')
  .then('src/libs/boot.js')
  .into(app);

module.exports = app;
