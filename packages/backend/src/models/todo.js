const todos = [];

/**
 * Add new todo
 * @todo {id, title, completed}
 */

exports.addTodo = todo => {
  const tobeTodo = { ...todo, completed: false };
  todos.push(tobeTodo);
  return tobeTodo;
};

/**
 * Update todo by Id
 * @param {todo: Todo}
 * @return {todo | false}
 */
exports.updateTodoById = todo => {
  let todoId = todos.findIndex(t => t.id == todo.id); // @return todo or undefined
  if (todoId !== -1) {
    todos[todoId] = { ...todos[todoId], ...todo };
    return todos[todoId];
  } else {
    return false;
  }
};

exports.deleteById = id => {
  const todoId = todos.findIndex(todo => todo.id === id); // index of todo // -1
  if (todoId === -1) return false;

  todos.splice(todoId, 1); // delete
  return true;
};

/**
 * find all todos
 * @param {completed}
 * return { list }
 */
exports.findAll = params => {
  const { completed } = params;
  if (!completed) {
    return todos;
  } else {
    return todos.filter(p => p.completed === completed);
  }
};
