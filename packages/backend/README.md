## TODO APP Back-End

### Build APIs:

- Create a item Todo: (`api/v1/createTodo`)
- Update Todo & mark this to do as completed (`api/v1/updateTodo`)
- Get All todos (`api/v1/todos`)
- Get all todos by completed state (`api/v1/todos`)
- Delete todo: (`api/v1/deleteTodo`)
- Clear completed todos (`api/clearCompleted`)
